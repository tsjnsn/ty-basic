/*
  Tyler Johnson
  tjohnson3@crimson.ua.edu
  --
  Recognizer:
    Builds parse trees.
*/

package tybasic;
import java.util.LinkedList;
public class Recognizer {
    Lexeme _current;
    Lexer _lexer;
	public Lexeme tree;
    public Recognizer(String args) {
        _lexer = new Lexer(args);
        advance();
        tree = start();
        //System.out.println("Legal");
    }

    public Lexeme start() {
        return program();
    }

    boolean check(Type type) {
        return (_current.getType() == type);
    }
	Type getType() {
		return (_current.getType());
	}
    Lexeme match(Type type) {
        //System.out.print(type + " ");
        Lexeme l = null;
        if(type == null || check(type))
            l = advance();
        else {
            //System.out.println(_current);
            System.out.println("Illegal");
            System.out.println("Expected type '" + type + "' but found type '" + _current.getType() +"'");
            System.exit(1);
        }
        return l;
    }

    Lexeme advance() {
		Lexeme c = _current;
		_current = _lexer.lex();
        return c;
    }
	
    //#######RECOGNIZER HELPERS###############################################
    Lexeme program() {
		Lexeme tree = block();
		match(Type.ENDOFINPUT);
		return tree;
    }

    Lexeme block() {
	   Lexeme declarations = optDeclList();
	   match(Type.END);
       return Lexeme.cons(Type.BLOCK, declarations, null);
    }
	

    Lexeme optDeclList() {
        if (declListPending())
            return declList();
        return null;
    }

    Lexeme declList() {
        Lexeme d = decl();
        Lexeme ds = null;
        if (declListPending()) {
			ds = declList();
        }
		return Lexeme.cons(Type.JOIN, d, ds);
    }

    Lexeme decl() {
        Lexeme d = null;
        if (funcDefPending())
            d = funcDef();
        else if (check(Type.IF))
            d = ifStatement();
        else if (check(Type.WHILE))
            d = whileLoop();
        else if (check(Type.FOR))
            d = forLoop();
        else if (check(Type.REPEAT))
            d = repeatLoop();
        else if (check(Type.RETURN))
            d = returnStatement();
		else if (check(Type.DISP))
            d = dispStatement();
        else if (check(Type.PAUSE))
            d = pauseStatement();
		
        else if (varDefPending())
            d = variableDefinition();
        else if (exprPending()) 
            d = expr();
		return d;
    }

    Lexeme funcDef() {
        Lexeme tree = match(Type.FUNCDEF);
        tree.left = match(Type.ID);
        match(Type.OPAREN);
        Lexeme list = optParamList();
        match(Type.CPAREN);
        //match(Type.COLON);
        Lexeme block = block();
        tree.right = Lexeme.cons(Type.JOIN, list, block);
		return tree;
    }
    Lexeme expr() {
        Lexeme tree = primary();
        if (opPending()) {
            Lexeme temp = op();
			temp.left = tree;
			temp.right = expr();
			tree = temp;
        }
		return tree;
    }
    Lexeme primary() {
        Lexeme tree = null;
        if (check(Type.INTEGER)) tree = match(Type.INTEGER);
        else if (check(Type.REAL)) tree = match(Type.REAL);
        else if (check(Type.STRING)) tree = match(Type.STRING);
		else if (check(Type.THIS)) tree = match(Type.THIS);
		else if (check(Type.BUILTIN)) tree = builtinStatement();
        else if (check(Type.OPAREN)) {
            tree = match(Type.OPAREN);
            tree.left = expr();
			tree.right = null;
            match(Type.CPAREN);
        }
        else if (check(Type.ID)) {
			tree = id();
		}
        return tree;
		//return Lexeme.cons(Type.PRIMARY, primary, null);
    }
	
	Lexeme id() {
		tree = match(Type.ID);
		if (check(Type.OBRACE)) {
			tree = array(tree);
		}
				//function call
		if (check(Type.OPAREN))
			tree = functionCall(tree);
				//variable assign
		else if (check(Type.ASSIGN))
			tree = varAssign(tree);
				//dot operator
		else if (check(Type.DOT))
			tree = dot(tree);
		return tree;
	}
	Lexeme array(Lexeme tree) {
		match(Type.OBRACE);
		Lexeme index = expr();
		match(Type.CBRACE);
		if (check(Type.ASSIGN)) {
			match(Type.ASSIGN);
			Lexeme val = expr();
			return Lexeme.cons(Type.ARRAYASSIGN, tree, Lexeme.cons(Type.JOIN, index, val));
		}
		return Lexeme.cons(Type.ARRAYREF, tree, index);
	}
	
	Lexeme dot(Lexeme id) {
		match(Type.DOT);
		return Lexeme.cons(Type.DOT, id, expr());
	}
	
    Lexeme varAssign(Lexeme id) {
		match(Type.ASSIGN);
		Lexeme value = expr();
        return Lexeme.cons(Type.ASSIGN, id, value);
    }
	
    Lexeme variableDefinition() {
        match(Type.DEFINE);
        Lexeme id = match(Type.ID);
		if (check(Type.OBRACE)) {
			match(Type.OBRACE);
			Lexeme size = expr();
			match(Type.CBRACE);
			return Lexeme.cons(Type.DEFINEARRAY, id, size);
		}
		Lexeme value = null;
        if (check(Type.ASSIGN)) {
            match(Type.ASSIGN);
            value = expr();
        }
		//return tree;
		
        return Lexeme.cons(Type.DEFINE, id, value);
    }
    Lexeme functionCall(Lexeme id) {
        //match(Type.CALL);
        //Lexeme id = match(Type.ID);
        match(Type.OPAREN);
        Lexeme args = optArgList();
        optCParen();
        return Lexeme.cons(Type.FUNCCALL, id, args);
    }
    void optCParen() {
        if (check(Type.CPAREN))
            match(Type.CPAREN);
    }
    Lexeme optArgList() {
        Lexeme args = null;
        if (argListPending())
            args = argList();
        return args;
    }
    Lexeme argList() {
        Lexeme value = expr();
        Lexeme args = null;
        if (check(Type.COMMA)) {
            match(Type.COMMA);
            args = argList();
        }
        return Lexeme.cons(Type.JOIN, value, args);
    }
    Lexeme op() {
        Lexeme op = null;
        if (check(Type.EQ)) op=match(Type.EQ);
        else if (check(Type.NEQ)) op=match(Type.NEQ);
        else if (check(Type.GT)) op=match(Type.GT);
        else if (check(Type.LT)) op=match(Type.LT);
        else if (check(Type.EGT)) op=match(Type.EGT);
        else if (check(Type.ELT)) op=match(Type.ELT);
        else if (check(Type.PLUS)) op=match(Type.PLUS);
        else if (check(Type.MINUS)) op=match(Type.MINUS);
        else if (check(Type.MULT)) op=match(Type.MULT);
        else if (check(Type.DIV)) op=match(Type.DIV);
        else if (check(Type.OR)) op=match(Type.OR);
        else if (check(Type.AND)) op=match(Type.AND);
        else if (check(Type.XOR)) op=match(Type.XOR);
        else if (check(Type.ASSIGN)) op=match(Type.ASSIGN);
        return op;
		//return Lexeme.cons(Type.OP, op, null);
    }
    Lexeme optParamList() {
       Lexeme params = null;
       if (paramListPending())
           params = paramList();
       return params;
    }
    Lexeme paramList() {
        Lexeme id = match(Type.ID);
        Lexeme plist = null;
        if (check(Type.COMMA)) {
            match(Type.COMMA);
            plist=paramList();
        }
        return Lexeme.cons(Type.JOIN, id, plist);
    }
    Lexeme ifStatement() {
        Lexeme tree = match(Type.IF);
        tree.left = expr();
        Lexeme thenDecl = null;
        Lexeme elseDecl = null;
        if (check(Type.THEN)) {
            match(Type.THEN);
            thenDecl = thenBlock();
			if (check(Type.ELSE)) {
				match(Type.ELSE);
				elseDecl = block();
			} else {
				match(Type.END);
			}
        }
        else {
            thenDecl = decl();
        }
        tree.right = Lexeme.cons(Type.JOIN, thenDecl, elseDecl);
		return tree;
		//return Lexeme.cons(Type.IF, chk, Lexeme.cons(Type.JOIN, thenDecl, elseDecl));
    }

	Lexeme thenBlock() {
	   Lexeme elseDecl=null;
	   Lexeme declarations = optDeclList();
	   
       return Lexeme.cons(Type.BLOCK, declarations, null);
    }
	
    Lexeme whileLoop() {
        match(Type.WHILE);
        Lexeme cond = expr();
        Lexeme code = block();
        return Lexeme.cons(Type.WHILE, cond, code);
    }
    Lexeme repeatLoop() {
        Lexeme tree = match(Type.REPEAT);
		tree.left = expr();
        tree.right = block();
        return tree;
    }
    Lexeme forLoop() {
        match(Type.FOR);
        Lexeme id, start, stop, step, code;
        match(Type.OPAREN);
        id = match(Type.ID);
        match(Type.COMMA);
        start = expr();
        match(Type.COMMA);
        stop = expr();
        step = optStep();
        optCParen();
		code = block();
        return Lexeme.cons(Type.FOR, id, Lexeme.cons(Type.JOIN, start, Lexeme.cons(Type.JOIN, stop, Lexeme.cons(Type.JOIN, step, code))));
    }
    Lexeme optStep() {
        Lexeme step = null;
        if (check(Type.COMMA)) {
            match(Type.COMMA);
            step = expr();
        }
        return step;
    }
	Lexeme dispStatement() {
        match(Type.DISP);
        Lexeme line = expr();
        return Lexeme.cons(Type.DISP, line, null);
    }
	Lexeme builtinStatement() {
        match(Type.BUILTIN);
		match(Type.OPAREN);
        Lexeme arg = expr();
		match(Type.CPAREN);
        return Lexeme.cons(Type.BUILTIN, arg, null);
    }
    Lexeme returnStatement() {
        match(Type.RETURN);
        Lexeme val = expr();
        return Lexeme.cons(Type.RETURN, val, null);
    }
    Lexeme pauseStatement() {
        match(Type.PAUSE);
        Lexeme disp = null;
        if (exprPending())
            disp = expr();
        return Lexeme.cons(Type.PAUSE, disp, null);
    }

    //#######PENDING FUNCTIONS################################################
    boolean declListPending() { return declPending(); }

    boolean declPending() {
        return (funcDefPending() || check(Type.IF) || varDefPending()
            || check(Type.WHILE) || check(Type.FOR) || check(Type.REPEAT) || check(Type.RETURN)
            || check(Type.PAUSE) || check(Type.DISP) ||check(Type.BUILTIN) || exprPending());
    }

    boolean funcDefPending() { return check(Type.FUNCDEF); }

    boolean varDefPending() { return check(Type.DEFINE); }

    boolean exprPending() {
        return primaryPending();
    }

    boolean primaryPending() {
        return (check(Type.INTEGER) || check(Type.REAL) || check(Type.STRING) || check(Type.ID)
            || check(Type.OPAREN));
    }
    

    boolean opPending() { 
        return (check(Type.EQ) || check(Type.NEQ) || check(Type.GT) || check(Type.LT)
            || check(Type.EGT) || check(Type.ELT) || check(Type.PLUS) || check(Type.MINUS)
            || check(Type.MULT) || check(Type.DIV) || check(Type.OR) || check(Type.AND)
            || check(Type.XOR) || check(Type.ASSIGN));
        }

    boolean paramListPending() {
        return check(Type.ID);
    }
    boolean argListPending() {
        return exprPending();
    }
}
