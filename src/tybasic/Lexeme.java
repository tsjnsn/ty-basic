/*
  Tyler Johnson
  tjohnson3@crimson.ua.edu
  --
  Lexeme:
    Basic data object for holding token data.
*/
package tybasic;

public class Lexeme {
    public Lexeme left, right;
    private Type _type;
    private String _string;
    private int _ival;
    private double _dval;
	private Lexeme _lval;
	private Lexeme[] _array;
	public Lexeme(Type type, Lexeme l) {
        _type = type;
        _lval = l;
    }
	public Lexeme(Type type, String string) {
        _type = type;
        _string = string;
    }
    public Lexeme(Type type, int ival) {
        _type = type;
        _ival = ival;
		
		if (_type == Type.ARRAY) {
			_array = new Lexeme[ival];
		}
    }
    public Lexeme(Type type, double dval) {
        _type = type;
        _dval = dval;
    }
    public Lexeme(Type type) {
        _type = type;
    }
    public Type getType() {
        return _type;
    }
    public String getString() {
        return _string;
    }
    public int getInt() {
        return _ival;
    }
    public double getDouble() {
        return _dval;
    }
	public Lexeme[] getArray() {
		return _array;
	}
	public Lexeme getLexeme() {
		return _lval;
	}
    /*
    public String toString() {
       if (_string != null)
        return _type + ":" + _string+"," + _ival+"," + _dval;
      return _type + ":," + _ival+"," + _dval;
    }*/
    public static Lexeme cons(Type type, Lexeme car, Lexeme cdr) {
        Lexeme l = new Lexeme(type);
        l.left = car;
        l.right = cdr;
        return l;
    }
    public String toString() {
        String s = "";
		if (_type == Type.CLOSURE) {
			s += "function <" + right.left._string + ">";
		}
		else if (_type == Type.ARRAY) {
			s += "[" + getInt() + "]";
		}
        else if (_type == Type.STRING) {
            s += _string;
		}
        else if (_type == Type.INTEGER || _type == Type.REAL) {
            if (_dval != 0)
                s += _dval;
            s += _ival;
        } else if (_type == Type.ENV) {
			s += "Object";
		} else if (_type == Type.ID) {
			s += "ID " + getString();
		} else {
			s+= "Type <"+ _type +">";
		}
        return s;
    }
}
