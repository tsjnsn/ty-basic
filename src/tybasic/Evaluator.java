/*
  Tyler Johnson
  tjohnson3@crimson.ua.edu
  --
  Evaluator:
    Breaks down parse trees & executes program.
*/
package tybasic;

public class Evaluator {
    Lexeme _env;
    public Evaluator() {
        _env = createEnv();
		Recognizer r = new Recognizer("builtins.tyb");
		eval(r.tree, _env);
    }
	
	//######## ENVIRONMENT #####################################################
    public Lexeme getEnv() {
        return _env;
    }
    public Lexeme createEnv() {
        return extend(null, null, null);
    }
    public Lexeme extend(Lexeme params, Lexeme args, Lexeme env) {
        return Lexeme.cons(Type.ENV, Lexeme.cons(Type.FRAME, params, args), env);
    }
    
    public Lexeme insertEnv(Lexeme id, Lexeme value, Lexeme env) {
        Lexeme frame = env.left;
        frame.left = Lexeme.cons(Type.JOIN, id, frame.left);
        frame.right = Lexeme.cons(Type.JOIN, value, frame.right);
        return value;
    }
	public Lexeme updateEnv(Lexeme id, Lexeme value, Lexeme env) {
		while (env != null) {
            Lexeme frame = env.left;
            Lexeme ids = frame.left;
            Lexeme vals = frame.right;
            while (ids != null) {
                if (id.getString().compareTo(ids.left.getString()) == 0) {
                    vals.left = value;
					return vals.left;
				}
                ids = ids.right;
                vals = vals.right;
            }
            env = env.right;
        }
        System.out.println("Error updating variable");
        System.exit(1);
        return null;
	}

    public Lexeme lookUp(Lexeme id, Lexeme env) {
        while (env != null) {
            Lexeme frame = env.left;
            Lexeme ids = frame.left;
            Lexeme vals = frame.right;
            while (ids != null) {
                if (id.getString().compareTo(ids.left.getString()) == 0) 
                    return vals.left;
                ids = ids.right;
                vals = vals.right;
            }
            env = env.right;
        }
        System.out.println(String.format("Error looking up '%s'", id));
        System.exit(1);
        return null;
    }
    public void printEnvironment(Lexeme env) {
        //Lexeme env = _env;
        //String s = "";
		String s = " _____________________________________________________\n";
        Lexeme frame = env.left;
        Lexeme ids = frame.left;
        Lexeme vals = frame.right;
        //s += "Environment extends " + _env + "\n";
        while(ids != null) {
            s += String.format("| %1$-25s|%2$25s |\n", ids.left, vals.left);
            //s += "|" + ids.left + " | " + vals.left + "|\n";
            ids = ids.right;
            vals = vals.right;
        }
        System.out.print(s);
        System.out.println("|_____________________________________________________|");
    }
    public void printEnvironments(Lexeme env) {
        //Lexeme env = _env;
        while (env != null) {
            printEnvironment(env);
            env = env.right;
        }
    }
	
	//########### END ENVIRONMENT CODE #########################################
	
	public Lexeme fatal(String message) {
		return new Lexeme(Type.FATAL, message);
	}
	
	public Lexeme eval(Lexeme tree, Lexeme env) {
		if (env.getType() == Type.RETURN) return null;
		if (tree == null) return null;
		//System.out.println("Evaluating type: " + tree.getType());
		switch (tree.getType()) {
		case FATAL: return null;
		case INTEGER: return tree;
		case REAL: return tree;
		case STRING: return tree;
		case ID: return lookUp(tree, env);
		case THIS: return env;
		//case OPAREN: return eval(tree.right, env);
		//case OP: return evalSimpleOp(tree, env);
		
		
		case PLUS: return evalSimpleOp(tree, env);
		case MINUS: return evalSimpleOp(tree, env);
		case MULT: return evalSimpleOp(tree, env);
		case DIV: return evalSimpleOp(tree, env);
		case EGT: return evalSimpleOp(tree, env);
		case ELT: return evalSimpleOp(tree, env);
		case GT: return evalSimpleOp(tree, env);
		case LT: return evalSimpleOp(tree, env);
		case EQ: return evalSimpleOp(tree, env);
		case NEQ: return evalSimpleOp(tree, env);
		
		
		case AND: return evalShortCircuitOp(tree, env);
		case OR: return evalShortCircuitOp(tree, env);
		case NOT: return evalShortCircuitOp(tree, env);
		
		//case DOT: return evalDot(tree, env);
		
		case ASSIGN: return evalAssign(tree, env);
		case ARRAYASSIGN: return evalArrayAssign(tree, env);
		case ARRAYREF: return evalArrayRef(tree, env);
		
		case BUILTIN: return evalBuiltin(tree, env);
		case DEFINE: return evalDefine(tree, env);
		case DEFINEARRAY: return evalDefineArray(tree, env);
		case FUNCDEF: return evalFuncDef(tree, env);
		case DOT: return evalDot(tree, env);
		
		case OPAREN: return evalOparen(tree, env);
		
		case DISP: return evalDisp(tree, env);
		case IF: return evalIf(tree, env);
		case WHILE: return evalWhile(tree, env);
		case REPEAT: return evalRepeat(tree, env);
		case FOR: return evalFor(tree, env);
		case FUNCCALL: return evalFuncCall(tree, env);
		case BLOCK: return evalBlock(tree, env);
		case RETURN: return evalReturn(tree, env);
		default: fatal("bad expression!");
		}
		return null;
	}
	
	Lexeme evalSimpleOp(Lexeme tree, Lexeme env) {
		if (tree.getType() == Type.PLUS) return evalPlus(tree, env);
		if (tree.getType() == Type.MINUS) return evalMinus(tree, env);
		if (tree.getType() == Type.DIV) return evalDiv(tree, env);
		if (tree.getType() == Type.MULT) return evalMult(tree, env);
		if (tree.getType() == Type.EGT) return evalEGT(tree, env);
		if (tree.getType() == Type.ELT) return evalELT(tree, env);
		if (tree.getType() == Type.GT) return evalGT(tree, env);
		if (tree.getType() == Type.LT) return evalLT(tree, env);
		if (tree.getType() == Type.EQ) return evalEQ(tree, env);
		if (tree.getType() == Type.NEQ) return evalNEQ(tree, env);
		return fatal("invalid operator");
	}
	
	Lexeme evalShortCircuitOp(Lexeme tree, Lexeme env) {
		if (tree.getType() == Type.AND) return evalAnd(tree, env);
		if (tree.getType() == Type.OR) return evalOr(tree, env);
		if (tree.getType() == Type.NOT) return evalNot(tree, env);
		return fatal("invalid operator");
	}
	
	Lexeme evalOparen(Lexeme tree, Lexeme env) {
		return eval(tree.left, env);
	}
	
	Lexeme evalArrayRef(Lexeme tree, Lexeme env) {
		Lexeme array = lookUp(tree.left, env);
		
		int index = eval(tree.right, env).getInt();
		
		int size = array.getInt();
		if (index < size && index >= 0) {
			/*for (int i=0;i<array.getArray().length;i++) {
				System.out.println("array:" + array.getArray()[i]);
			}*/
			return array.getArray()[index];
		} else {
			return fatal("index out of bounds");
		}
	}
	
	Lexeme evalAssign(Lexeme tree, Lexeme env) {
		Lexeme val = eval(tree.right, env);
		updateEnv(tree.left, val, env);
		//printEnvironments(env);
		return val;
	}
	
	Lexeme evalArrayAssign(Lexeme tree, Lexeme env) {
		Lexeme val = eval(tree.right.right, env);
		Lexeme array = lookUp(tree.left, env);
		
		int index = eval(tree.right.left, env).getInt();
		int size = array.getInt();
		
		if (index < size && index >= 0) {
			array.getArray()[index] = val;
		}
		else
			return fatal("index out of bounds");
		
		//printEnvironments(env);
		return val;
	}
	
	Lexeme evalDefine(Lexeme tree, Lexeme env) {
		Lexeme val = eval(tree.right, env);
		insertEnv(tree.left, val, env);
		//printEnvironments(env);
		return val;
	}
	
	Lexeme evalDefineArray(Lexeme tree, Lexeme env) {
		Lexeme size = eval(tree.right, env);
		Lexeme ar = new Lexeme(Type.ARRAY, size.getInt());
		insertEnv(tree.left, ar, env);
		//printEnvironments(env);
		return ar;
	}
	
	Lexeme getClosureParams(Lexeme tree) { return tree.right.right.left; }
	Lexeme getClosureBody(Lexeme tree) { return tree.right.right.right; }
	Lexeme getClosureEnv(Lexeme tree) { return tree.left; }
	
			Lexeme getFuncDefName(Lexeme tree) { return tree.left; }
			Lexeme getFuncDefParams(Lexeme tree) { return tree.right.left; }
			Lexeme getFuncDefBody(Lexeme tree) { return tree.right.right; }
	Lexeme evalFuncDef(Lexeme tree, Lexeme env) {
		Lexeme closure = Lexeme.cons(Type.CLOSURE, env, tree);
		insertEnv(getFuncDefName(tree), closure, env);
		//printEnvironments(env);
		return closure;
	}
			Lexeme getFuncCallName(Lexeme tree) { return tree.left; }
			Lexeme getFuncCallArgs(Lexeme tree) { return tree.right; }
	Lexeme evalFuncCall(Lexeme tree, Lexeme env) {
		Lexeme closure = eval(getFuncCallName(tree), env);
		Lexeme args = getFuncCallArgs(tree);
		Lexeme params = getClosureParams(closure);
		Lexeme body = getClosureBody(closure);
		Lexeme senv = getClosureEnv(closure);
		//printEnvironment(env);
		Lexeme eargs = evalArgs(args, env);
		Lexeme xenv = extend(params, eargs, senv);
		//printEnvironments(env);
		Lexeme ret = eval(body, xenv);
		if (ret.getType() == Type.RETURN)
			return ret.left;
		return ret;
	}
	
	Lexeme evalBuiltin(Lexeme tree, Lexeme env) {
		Lexeme arg = eval(tree.left, env);
		String[] command = arg.getString().split(" ");
		
        String f = command[0];
        String s;
		if (f.compareTo("length") == 0) {
			if (command.length == 1) return new Lexeme(Type.INTEGER, 0);
			String lstr = command[1];
			return new Lexeme(Type.INTEGER, lstr.length());
        }
		else if (f.compareTo("size") == 0) {
			String astr = command[1];
			return new Lexeme(Type.INTEGER, Integer.parseInt(astr.substring(1,2)));
        }
		else if (f.compareTo("str2int") ==0 ) {
			s = command[1];
			return new Lexeme(Type.INTEGER, Integer.parseInt(s));
        }
		else if (f.compareTo("str2real") ==  0) {
			s = command[1];
			return new Lexeme(Type.REAL, Double.parseDouble(s));
        }
		else if (f.compareTo("int2str") == 0) {
			s = command[1];
			return new Lexeme(Type.STRING, ""+s);
        }
		else if (f.compareTo("substr") == 0) {
			s = command[1];
			int start = Integer.parseInt(command[2]);
			int end = Integer.parseInt(command[3]);
			if (start == end) return new Lexeme(Type.STRING, "");
			return new Lexeme(Type.STRING, s.substring(start, end));
        }
		else if (f.compareTo("strcmp") == 0) {
			s = command[1];
			String str = command[2];
			return newBooleanLexeme(s.compareTo(str) == 0);
		}
		return fatal("builtin not recognized");
	}
	
	Lexeme evalDisp(Lexeme tree, Lexeme env) {
		Lexeme message = eval(tree.left, env);
		System.out.println(message);
		return message;
	}
	
	Lexeme evalArgs(Lexeme tree, Lexeme env) {
		Lexeme temp = new Lexeme(Type.JOIN);
		Lexeme eargs = temp;
		while (tree != null) {
			temp.left = eval(tree.left, env);
			tree = tree.right;
			if (tree != null) {
				temp.right = new Lexeme(Type.JOIN);
				temp = temp.right;
			}
		}
		return eargs;
	}
	
	Lexeme evalIf(Lexeme tree, Lexeme env) {
		Lexeme cond = eval(tree.left, env);
		Lexeme thenDecls = tree.right.left;
		Lexeme elseDecls = tree.right.right;
		Lexeme r = null;
		if (cond.getType() == Type.INTEGER && cond.getInt() != 0) {
			r = eval(thenDecls, env);
		}
		else if (cond.getType() == Type.INTEGER && cond.getInt() == 0 && elseDecls != null) {
			r = eval(elseDecls, env);
		}
		return r;
	}
	Lexeme evalWhile(Lexeme tree, Lexeme env) {
		Lexeme cond = eval(tree.left, env);
		Lexeme block = tree.right;
		Lexeme r = null;
		while (cond.getType() == Type.INTEGER && cond.getInt() != 0) {
			r = eval(tree.right, env);
			cond = eval(tree.left, env);
		}
		return r;
	}
	Lexeme evalRepeat(Lexeme tree, Lexeme env) {
		Lexeme cond = eval(tree.left, env);
		Lexeme block = tree.right;
		Lexeme r = null;
		while (cond.getType() == Type.INTEGER && cond.getInt() == 0) {
			r = eval(tree.right, env);
			cond = eval(tree.left, env);
		}
		return r;
	}
	Lexeme evalFor(Lexeme tree, Lexeme env) {
		Lexeme v = tree.left;
		Lexeme start = eval(tree.right.left, env);
		updateEnv(v, start, env);  //set inital value
		Lexeme r = null;
		int stop = eval(tree.right.right.left, env).getInt();
		int step = 1;
		if (tree.right.right.right.left != null)
			step = eval(tree.right.right.right.left, env).getInt();
		for (;eval(v, env).getInt()<=stop; updateEnv(v, new Lexeme(Type.INTEGER, eval(v, env).getInt()+step), env)) {
			r = eval(tree.right.right.right.right, env);
		}
		return r;
	}
	
	Lexeme evalBlock(Lexeme tree, Lexeme env) {
		Lexeme list = tree.left;
		Lexeme rval = null;
		while (list != null) {
			rval = eval(list.left, env);
			if (rval != null && rval.getType() == Type.RETURN) {
				break;
			}
			list = list.right;
		}
		return rval;
	}
	
	Lexeme evalDot(Lexeme tree, Lexeme env) {
		Lexeme object = eval(tree.left, env);
		Lexeme scopeditem = tree.right;
		if (scopeditem.getType() == Type.FUNCCALL) {
			scopeditem.right = evalArgs(scopeditem.right, env);
		}
		return eval(scopeditem, object);
	}
	
	Lexeme evalReturn(Lexeme tree, Lexeme env) {
		return Lexeme.cons(Type.RETURN, eval(tree.left, env), null);
	}
	
	Lexeme evalPlus(Lexeme tree, Lexeme env) {
		Lexeme left = eval(tree.left, env);
		Lexeme right = eval(tree.right, env);
		if (left == null || right == null) return fatal("null pointer exception");
		if (left.getType() == Type.INTEGER && right.getType() == Type.INTEGER)
			return new Lexeme(Type.INTEGER, left.getInt() + right.getInt());
		else if (left.getType() == Type.INTEGER && right.getType() == Type.REAL)
			return new Lexeme(Type.REAL, left.getInt() + right.getDouble());
		else if (left.getType() == Type.REAL && right.getType() == Type.INTEGER)
			return new Lexeme(Type.REAL, left.getDouble() + right.getInt());
		else if (left.getType() == Type.STRING || right.getType() == Type.STRING)
			return new Lexeme(Type.STRING, "" + left + right);
		return fatal("invalid 'plus'");
	}
	
	Lexeme evalMinus(Lexeme tree, Lexeme env) {
		Lexeme left = eval(tree.left, env);
		Lexeme right = eval(tree.right, env);
		if (left == null || right == null) return fatal("null pointer exception");
		if (left.getType() == Type.INTEGER && right.getType() == Type.INTEGER)
			return new Lexeme(Type.INTEGER, left.getInt() - right.getInt());
		else if (left.getType() == Type.INTEGER && right.getType() == Type.REAL)
			return new Lexeme(Type.REAL, left.getInt() - right.getDouble());
		else if (left.getType() == Type.REAL && right.getType() == Type.INTEGER)
			return new Lexeme(Type.REAL, left.getDouble() - right.getInt());
		else if (left.getType() == Type.STRING && right.getType() == Type.INTEGER)
			return new Lexeme(Type.STRING, left.getString().substring(1));
		return fatal("invalid 'minus'");
	}
	Lexeme evalMult(Lexeme tree, Lexeme env) {
		Lexeme left = eval(tree.left, env);
		Lexeme right = eval(tree.right, env);
		if (left == null || right == null) return fatal("null pointer exception");
		if (left.getType() == Type.INTEGER && right.getType() == Type.INTEGER)
			return new Lexeme(Type.INTEGER, left.getInt() * right.getInt());
		else if (left.getType() == Type.INTEGER && right.getType() == Type.REAL)
			return new Lexeme(Type.REAL, left.getInt() * right.getDouble());
		else if (left.getType() == Type.REAL && right.getType() == Type.INTEGER)
			return new Lexeme(Type.REAL, left.getDouble() * right.getInt());
		return fatal("invalid 'multiply'");
	}
	Lexeme evalDiv(Lexeme tree, Lexeme env) {
		Lexeme left = eval(tree.left, env);
		Lexeme right = eval(tree.right, env);
		if (left == null || right == null) return fatal("null pointer exception");
		if (left.getType() == Type.INTEGER && right.getType() == Type.INTEGER)
			return new Lexeme(Type.INTEGER, left.getInt() / right.getInt());
		else if (left.getType() == Type.INTEGER && right.getType() == Type.REAL)
			return new Lexeme(Type.REAL, left.getInt() / right.getDouble());
		else if (left.getType() == Type.REAL && right.getType() == Type.INTEGER)
			return new Lexeme(Type.REAL, left.getDouble() / right.getInt());
		return fatal("invalid 'divide'");
	}
	
	Lexeme newBooleanLexeme(boolean cond) {
		if (cond)
			return new Lexeme (Type.INTEGER, 1);
		else return new Lexeme (Type.INTEGER, 0);
	}
	
	Lexeme evalEGT(Lexeme tree, Lexeme env) {
		
		Lexeme left = eval(tree.left, env);
		Lexeme right = eval(tree.right, env);
		if (left == null || right == null) return fatal("null pointer exception");
		if (left.getType() == Type.INTEGER && right.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getInt() >= right.getInt());
		else if (left.getType() == Type.INTEGER && right.getType() == Type.REAL)
			return newBooleanLexeme(left.getInt() >= right.getDouble());
		else if (left.getType() == Type.REAL && right.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getDouble() >= right.getInt());
		return fatal("invalid 'greater than or equal to'");
	}
	Lexeme evalELT(Lexeme tree, Lexeme env) {
		Lexeme left = eval(tree.left, env);
		Lexeme right = eval(tree.right, env);
		if (left == null || right == null) return fatal("null pointer exception");
		if (left.getType() == Type.INTEGER && right.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getInt() <= right.getInt());
		else if (left.getType() == Type.INTEGER && right.getType() == Type.REAL)
			return newBooleanLexeme(left.getInt() <= right.getDouble());
		else if (left.getType() == Type.REAL && right.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getDouble() <= right.getInt());
		return fatal("invalid 'less than or equal to'");
	}
	Lexeme evalGT(Lexeme tree, Lexeme env) {
		Lexeme left = eval(tree.left, env);
		Lexeme right = eval(tree.right, env);
		if (left == null || right == null) return fatal("null pointer exception");
		if (left.getType() == Type.INTEGER && right.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getInt() > right.getInt());
		else if (left.getType() == Type.INTEGER && right.getType() == Type.REAL)
			return newBooleanLexeme(left.getInt() > right.getDouble());
		else if (left.getType() == Type.REAL && right.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getDouble() > right.getInt());
		return fatal("invalid 'greater than'");
	}
	Lexeme evalLT(Lexeme tree, Lexeme env) {
		
		Lexeme left = eval(tree.left, env);
		Lexeme right = eval(tree.right, env);
		if (left == null || right == null) return fatal("null pointer exception");
		if (left.getType() == Type.INTEGER && right.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getInt() < right.getInt());
		else if (left.getType() == Type.INTEGER && right.getType() == Type.REAL)
			return newBooleanLexeme(left.getInt() < right.getDouble());
		else if (left.getType() == Type.REAL && right.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getDouble() < right.getInt());
		return fatal("invalid 'less than'");
	}
	Lexeme evalEQ(Lexeme tree, Lexeme env) {
		Lexeme left = eval(tree.left, env);
		Lexeme right = eval(tree.right, env);
		if (left == null || right == null) return fatal("null pointer exception");
		if (left.getType() == Type.INTEGER && right.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getInt() == right.getInt());
		else if (left.getType() == Type.INTEGER && right.getType() == Type.REAL)
			return newBooleanLexeme(left.getInt() == right.getDouble());
		else if (left.getType() == Type.REAL && right.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getDouble() == right.getInt());
		else if (left.getType() == Type.STRING && right.getType() == Type.STRING)
			return newBooleanLexeme(left.getString() == right.getString());
		return fatal("invalid 'equal to'");
	}
	Lexeme evalNEQ(Lexeme tree, Lexeme env) {
		
		Lexeme left = eval(tree.left, env);
		Lexeme right = eval(tree.right, env);
		if (left == null || right == null) return fatal("null pointer exception");
		if (left.getType() == Type.INTEGER && right.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getInt() != right.getInt());
		else if (left.getType() == Type.INTEGER && right.getType() == Type.REAL)
			return newBooleanLexeme(left.getInt() != right.getDouble());
		else if (left.getType() == Type.REAL && right.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getDouble() != right.getInt());
		else if (left.getType() == Type.STRING && right.getType() == Type.STRING)
			return newBooleanLexeme(left.getString() != right.getString());
		return fatal("invalid 'not equal to'");
	}
	Lexeme evalAnd(Lexeme tree, Lexeme env) {
		Lexeme left = eval(tree.left, env);
		Lexeme right = eval(tree.right, env);
		if (left == null || right == null) return fatal("null pointer exception");
		if (left.getType() == Type.INTEGER && right.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getInt()!=0 && right.getInt()!=0);
		else if (left.getType() == Type.INTEGER && right.getType() == Type.REAL)
			return newBooleanLexeme(left.getInt()!=0 && right.getDouble()!=0);
		else if (left.getType() == Type.REAL && right.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getDouble()!=0 && right.getInt()!=0);
		return fatal("invalid 'and'");
	}
	Lexeme evalOr(Lexeme tree, Lexeme env) {
		Lexeme left = eval(tree.left, env);
		Lexeme right = eval(tree.right, env);
		if (left == null || right == null) return fatal("null pointer exception");
		if (left.getType() == Type.INTEGER && right.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getInt()!=0 || right.getInt()!=0);
		else if (left.getType() == Type.INTEGER && right.getType() == Type.REAL)
			return newBooleanLexeme(left.getInt()!=0 || right.getDouble()!=0);
		else if (left.getType() == Type.REAL && right.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getDouble()!=0 || right.getInt()!=0);
		return fatal("invalid 'or'");
	}
	Lexeme evalNot(Lexeme tree, Lexeme env) {
		Lexeme left = eval(tree.left, env);
		if (left == null) return fatal("null pointer exception");
		if (left.getType() == Type.INTEGER)
			return newBooleanLexeme(left.getInt()==0);
		else if (left.getType() == Type.REAL)
			return newBooleanLexeme(left.getDouble()==0);
		return fatal("invalid 'not'");
	}
	
}
