/*
  Tyler Johnson
  tjohnson3@crimson.ua.edu
  --
  Lexer:
    Takes a given file upon creation.
    Repeated calls to lex() will return Lexemes.
*/
package tybasic;
import java.io.*;

public class Lexer {
  private BufferedReader _br;
  public Lexer(String filename) {
    try {
      _br = new BufferedReader(new FileReader(filename));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  private int read() {
    try {
      _br.mark(1);
      return _br.read();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return -1;
  }
  private void pushBack() {
    try {
      _br.reset();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  void skipWhiteSpace() {
    int n = read();
    if (n == -1) {
      pushBack();
      return;
    }
    char ch = (char) n;
    if (Character.isWhitespace(ch)) {//ch == ' ' || ch == '\n' || ch == '\t') {
      skipWhiteSpace();
      return;
    }
    if (ch == '#') {
      skipComment();
      skipWhiteSpace();
      return;
    }
    pushBack();
  }

  void skipComment() {
    int n = read();
    if (n == -1) {
      pushBack();
      return;
    }
    char ch = (char) n;
    if (ch == '\n') {
      pushBack();
      return;
    }
    skipComment();
  }

  public Lexeme lex() {
    skipWhiteSpace();
    int n = read();
    char ch = (char) n;
    char c2;
    if (n == -1)
      return new Lexeme(Type.ENDOFINPUT);
    switch(ch) {
	  case '.':
		return new Lexeme(Type.DOT);
      case '(':
        return new Lexeme(Type.OPAREN);
      case ')':
        return new Lexeme(Type.CPAREN);
	  case '[':
        return new Lexeme(Type.OBRACE);
      case ']':
        return new Lexeme(Type.CBRACE);
      case ',':
        return new Lexeme(Type.COMMA);
      case '+':
        return new Lexeme(Type.PLUS);
      case '-':
        return new Lexeme(Type.MINUS);
      case '*':
        return new Lexeme(Type.MULT);
      case '/':
        return new Lexeme(Type.DIV);
	  case '!':
		return new Lexeme(Type.NOT);
      case '>':
        c2 = (char)read();
        if (c2 == '=')
          return new Lexeme(Type.EGT);
        return new Lexeme(Type.GT);
      case '<':
        c2 = (char)read();
        if (c2 == '<')
          return new Lexeme(Type.ASSIGN);
        if (c2 == '=')
          return new Lexeme(Type.ELT);
        pushBack();
        return new Lexeme(Type.LT);
      case '=':
        return new Lexeme(Type.EQ);
      //case '\n':
      //  return new Lexeme(Type.NEWLINE);

      default:
        if (ch == '!') {
          if ((char)read() == '=')
            return new Lexeme(Type.NEQ);
          pushBack();
        }
        if (ch == '\"') {
          return lexString();
        }
         
        if (Character.isDigit(ch)) {
          pushBack();
          return lexNumber();
        }
        else if (Character.isLetter(ch)) {
          pushBack();
          return lexVariable();
        } 
        else {
          return new Lexeme(Type.NULL);
        }
    }
  }
  public Lexeme lexString() {
    int n = read();
    char ch = (char) n;
    String str = "";
    while (n != -1 && (ch != '\"' && ch != '\n')) {
      str += ch;
      n = read();
      ch = (char)n;
    }
    if (ch == '\n')
      pushBack();
    return new Lexeme(Type.STRING, str);
  }
  public Lexeme lexVariable() {
    int n = read();
    char ch = (char) n;
    String id = "";
    while (n != -1 && (Character.isLetter(ch) || (Character.isDigit(ch)))) {
      id += ch;
      n = read();
      ch = (char)n;
    }
    pushBack();
    if (id.equalsIgnoreCase("function"))
      return new Lexeme(Type.FUNCDEF);
    if (id.equalsIgnoreCase("if"))
      return new Lexeme(Type.IF);
    if (id.equalsIgnoreCase("then"))
      return new Lexeme(Type.THEN);
    if (id.equalsIgnoreCase("else"))
      return new Lexeme(Type.ELSE);
    if (id.equalsIgnoreCase("end"))
      return new Lexeme(Type.END);
    if (id.equalsIgnoreCase("for"))
      return new Lexeme(Type.FOR);
    if (id.equalsIgnoreCase("while"))
      return new Lexeme(Type.WHILE);
    if (id.equalsIgnoreCase("repeat"))
      return new Lexeme(Type.REPEAT);
    if (id.equalsIgnoreCase("return"))
      return new Lexeme(Type.RETURN);
    if (id.equalsIgnoreCase("input"))
      return new Lexeme(Type.INPUT);
    if (id.equalsIgnoreCase("define"))
      return new Lexeme(Type.DEFINE);
    if (id.equalsIgnoreCase("call"))
      return new Lexeme(Type.CALL);
	if (id.equalsIgnoreCase("or"))
      return new Lexeme(Type.OR);
	if (id.equalsIgnoreCase("and"))
      return new Lexeme(Type.AND);
	if (id.equalsIgnoreCase("this"))
      return new Lexeme(Type.THIS);
    if (id.equalsIgnoreCase("builtin"))
      return new Lexeme(Type.BUILTIN);
    if (id.equalsIgnoreCase("disp"))
      return new Lexeme(Type.DISP);
    if (id.equalsIgnoreCase("pause"))
      return new Lexeme(Type.PAUSE);
    if (id.equalsIgnoreCase("ans"))
      return new Lexeme(Type.ANS);
    return new Lexeme(Type.ID,id);
  }
  public Lexeme lexNumber() {
    int n = read();
    char ch = (char) n;
    String num = "";
    boolean isInt = true;
    while (n != -1){
      if (Character.isDigit(ch)) {
        num = num + ch;
      } else if (ch == '.') {
        if (!isInt) {
          pushBack();
          break;
        } else {
          isInt = false;
          num = num + ch;
        }
      } else {
        pushBack();
        break;
      }
      n = read();
      ch = (char) n;
    }
    if (isInt)
      return new Lexeme(Type.INTEGER, Integer.parseInt(num));
    else
      return new Lexeme(Type.REAL, Double.parseDouble(num));
  }

  void print( ) {
    System.out.println((char)read());
    pushBack();
  }
}
