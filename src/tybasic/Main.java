/*
  Tyler Johnson
  tjohnson3@crimson.ua.edu
  --
  Main Class:
    The "Scanner."  This is used for testing purposes.
*/

package tybasic;

class Scanner {
    public Scanner(String arg) {
        Lexer lexer = new Lexer(arg);
        while (true) {
            Lexeme l = lexer.lex();
            if (l.getType() == Type.NULL) {
              System.out.println("UNRECOGNIZED LEXEME");
            } else 
            if (l.getType() == Type.ENDOFINPUT) {
              System.out.println("End of input");
              break;
            } else {
              Type t = l.getType();
              if (t == Type.INTEGER) {
                System.out.println(t + "," + l.getInt());
              } else
              if (t == Type.REAL) {
                System.out.println(t + "," + l.getDouble());
              } else
              if (t == Type.STRING) {
                System.out.println(t + "," + l.getString());
              } else {
                System.out.println(t);
              }
            }
          }
    }
}

public class Main {
  
	public static void printParseTree(Lexeme tree, String prefix) {
		if (tree == null) return;
		System.out.println(prefix + tree.getType());
		printParseTree(tree.left, prefix + "\t");
		printParseTree(tree.right, prefix);
	}
  
  public static void main(String[] args) {
    String command = args[0];
    for (int i=1; i<args.length; i++) {
      if (command.equalsIgnoreCase("scanner")) {
        new Scanner(args[i]);
      }
      else if (command.equalsIgnoreCase("recognizer")) {
        new Recognizer(args[i]);
      }
	  else if (command.equalsIgnoreCase("evaluator")) {
        Evaluator e = new Evaluator();
		Recognizer r = new Recognizer(args[i]);
		
		//printParseTree(r.tree, "");
		
		e.eval(r.tree, e.getEnv());
      }
    }
    if (command.equalsIgnoreCase("environment")) {
        //Lexeme tree = Recognizer(args[i]);
        Evaluator e = new Evaluator();
        Lexeme env = e.getEnv();
        System.out.println("Creating a new environment");
        System.out.println("The new environment is:");
        e.printEnvironment(env);
        
        System.out.println("Adding variables (x=50, HelloText=\"heelo\", arealnumber=6.26): ");
        System.out.println("The environment is now:");
        e.insertEnv(new Lexeme(Type.ID, "x"), new Lexeme(Type.INTEGER, 50), env);
        e.insertEnv(new Lexeme(Type.ID, "HelloText"), new Lexeme(Type.STRING, "heelo"), env);
        e.insertEnv(new Lexeme(Type.ID, "arealnumber"), new Lexeme(Type.INTEGER, 6.26), env);
        e.printEnvironment(env);

        System.out.println("Extending the environment with (x=5): ");
        Lexeme env2 = e.extend(
            Lexeme.cons(Type.JOIN, new Lexeme(Type.ID, "x"), null),
            Lexeme.cons(Type.JOIN, new Lexeme(Type.INTEGER, 5), null),
            env);
        System.out.println("The local environment is:");
        e.printEnvironment(env2);
        System.out.println("The environment is:");
        e.printEnvironment(env);

        System.out.print("Looking up variable 'x' in the local environment: ");
        System.out.println(e.lookUp(new Lexeme(Type.ID, "x"), env2));
        
        System.out.print("Looking up variable 'HelloText' in the local environment: ");
        System.out.println(e.lookUp(new Lexeme(Type.ID, "HelloText"), env2));

        System.out.print("Looking up variable 'x' in the global environment: ");
        System.out.println(e.lookUp(new Lexeme(Type.ID, "x"), env));
        
		System.out.print("Updating the variable 'x' to be 7890");
		e.updateEnv(new Lexeme(Type.ID, "x"), new Lexeme(Type.INTEGER, 7890), env);
		System.out.println("The environment is now:");
		e.printEnvironment(env);
        
        System.out.println("All environments:");
        e.printEnvironments(env2);
      }
        //e.add((new Lexeme(Type.ID, "x")).getString(), new Lexeme(Type.INTEGER, 25));

  }
}
