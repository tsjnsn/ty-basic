/*
  Tyler Johnson
  tjohnson3@crimson.ua.edu
  --
  Type:
    A Java enumeration containing all different
    possible lexeme types.
*/
package tybasic;

public enum Type{
  //punctuation
    OPAREN,
    CPAREN,
    
    DEFINE,
    CALL,
    //COLON,
    COMMA,
    //NEWLINE,
    ID,
    ASSIGN,
    FUNCDEF,
    
    //Operations
    EQ,
    NEQ,
    LT,
    GT,
    ELT,
    EGT,

    PLUS,
    MINUS,
    MULT,
    DIV,

    OR,
    AND,
    XOR,
    NOT,
    
    //Data Types
    INTEGER,
    REAL,
    STRING,
    LIST,

    //Program Control
    IF,
    THEN,
    ELSE,
    
    WHILE,
    REPEAT,
    FOR,
    
    END,
    RETURN,

    //I/O
    PAUSE,
    INPUT,
    PROMPT,
    DISP,
    
    ANS,

    ENDOFINPUT,
    NULL,


    //PARSE TREE
    JOIN,
    DOT,
    FUNCCALL,
    ARRAY,
    OBRACE,
    CBRACE,
    DEFINEARRAY,
    THIS,
    BLOCK,
	CLOSURE,
	FATAL,
	ARRAYASSIGN,
	ARRAYREF,
	BUILTIN,
    
    //Environment
    ENV,
    FRAME;
}
