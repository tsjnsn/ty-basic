#Tyler Johnson
#tjohnson3@crimson.ua.edu
#
#

CLASSPATH=classes
SOURCEPATH=src

all:
	javac -cp $(CLASSPATH) -d $(CLASSPATH) -sourcepath $(SOURCEPATH) src/tybasic/Main.java
rec:
	javac -cp $(CLASSPATH) -d $(CLASSPATH) -sourcepath $(SOURCEPATH) src/tybasic/Recognizer.java
runrec: 
	./recognizer tyb_testfiles/test_legal_00.tyb
	./recognizer tyb_testfiles/test_legal_01.tyb
	./recognizer tyb_testfiles/test_legal_02.tyb
	./recognizer tyb_testfiles/test_illegal_00.tyb
	./recognizer tyb_testfiles/test_illegal_01.tyb
run: array object conditional recursion iteration rpn
array:
	java -cp classes tybasic.Main evaluator tyb_testfiles/array.tyb
object:
	java -cp classes tybasic.Main evaluator tyb_testfiles/object.tyb
conditional:
	java -cp classes tybasic.Main evaluator tyb_testfiles/conditional.tyb
recursion:
	java -cp classes tybasic.Main evaluator tyb_testfiles/recursion.tyb
iteration:
	java -cp classes tybasic.Main evaluator tyb_testfiles/iteration.tyb
rpn:
	java -cp classes tybasic.Main evaluator tyb_testfiles/rpn.tyb
clean:
	rm -r $(CLASSPATH)/tybasic
