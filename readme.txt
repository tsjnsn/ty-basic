scanner:
./scanner <filename>

recognizer:
./recognizer <filename>

example set of commands to test recognizer:
make clean
make

*The program looks for the builtin.tyb file in the same directory in which it is called.


Notes:


DATA TYPES

	integer, real, string
	
	*booleans are integers.  0 is false, anything else is true
	*there is no unary minus, use 0-x

    arrays:
        simply add brackets and a size to variables when declaring them
    define a[25]


VARIABLES
	
	define <variable name>
	--OR--
	define <variable name> << <initial value expression>
	<< (this is assignment)
	<id> << <expr>
	
	to define an array:
	define <variable name>[<size>]
	
	examples:
	define x
	define x << 5
	define x[10]
	x[1] << 5


FLOW CONTROL
	
	if <cond>
	<single-line predicate>
	--OR--
	if <cond>
	then <predicate>
	end
	--OR--
	if <cond>
	then <predicate>
	else <elsepredicate>
	end

	while <cond> <body>
	end

	repeat <cond> <body>
	end
	
	for(<id>, <intlow>, <inthigh>, <?intstep>) <body>
	end
	
	*I removed label and goto because I couldn't figure out how to do them.
	
	
	
OPERATORS
	
	and
	or
	=
	!=
	>=
	<=
	+
	-
	/
	*
	
	*binary operators take the form <expr> op <expr>
	
	not <expr>
	
	
CLASSES & OBJECTS
	
	*I couldn't quite get this part to work correctly.  There is limited functionality.
	*In summary, you can call functions in other objects, but the arguments evaluate in the
	*environment of the called function, rather than in the calling environment.
	
	*You must have 'return this' as the last statement in a function in order for it to serve as an object
	example:
	
	function node(value)
		define next
		return this
	end
	define a << node(20)
	disp a.value

